golang-google-appengine (1.6.0-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 19:24:41 +0000

golang-google-appengine (1.6.0-1) unstable; urgency=medium

  * New upstream version 1.6.0
  * Update Maintainer email address to team+pkg-go@tracker.debian.org
  * Bump Standards-Version to 4.4.0 (no change)
  * Use "Build-Depends: debhelper-compat (= 12)" for debhelper dependency

 -- Anthony Fok <foka@debian.org>  Sun, 28 Jul 2019 06:13:35 -0600

golang-google-appengine (1.4.0-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 15 Feb 2021 12:12:52 +0000

golang-google-appengine (1.4.0-1) unstable; urgency=medium

  * New upstream version 1.4.0
  * Remove uupdate from debian/watch
  * Bump Standards-Version to 4.3.0 (no change)
  * Use debhelper (>= 12~)
  * Update versioned dependencies according to go.mod

 -- Anthony Fok <foka@debian.org>  Tue, 01 Jan 2019 22:50:36 -0700

golang-google-appengine (1.1.0-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Anthony Fok ]
  * New upstream version 1.1.0
  * Apply "cme fix dpkg" fixes to debian/control,
    setting package Priority to optional,
    bumping Standards-Version to 4.1.4 (no change), etc.
  * Fix debian/watch

 -- Anthony Fok <foka@debian.org>  Tue, 26 Jun 2018 03:21:10 -0600

golang-google-appengine (1.0.0-3) unstable; urgency=medium

  * Update dependency package names.
  * Update version of dh-golang dependency (for XS-Go-Import-Path
    support).
  * Remove Built-Using from arch:all -dev package
  * debian/control: Update Standards-Version (no changes).
  * debian/control: Mark package as autopkgtest-able.
  * debian/control: Remove references to obsolete golang-go.net-dev.
  * debian/copyright: Format uses http protocol instead of https.

 -- Martín Ferrari <tincho@debian.org>  Mon, 28 Aug 2017 03:37:23 +0000

golang-google-appengine (1.0.0-2) unstable; urgency=medium

  * Add myself to Uploaders.
  * Set API_HOST to localhost to avoid network access during tests.
    Closes: #840972.

 -- Martín Ferrari <tincho@debian.org>  Fri, 21 Oct 2016 17:37:26 +0000

golang-google-appengine (1.0.0-1) unstable; urgency=medium

  [ Paul Tagliamonte ]
  * Team upload.
  * Use a secure transport for the Vcs-Git and Vcs-Browser URL

  [ Anthony Fok ]
  * New upstream release
  * Refresh debian/control:
    - Bump Standards-Version to 3.9.8
    - Replace golang-go with golang-any in Build-Depends
    - Use secure https URI for Vcs-Browser too
    - Add XS-Go-Import-Path field (and remove DH_GOPKG from debian/rules)
    - Add myself to the list of Uploaders
  * Add debian/watch file

 -- Anthony Fok <foka@debian.org>  Sat, 15 Oct 2016 05:45:32 -0600

golang-google-appengine (0.0~git20150606-2) unstable; urgency=medium

  * Add missing Depends

 -- Michael Stapelberg <stapelberg@debian.org>  Mon, 15 Jun 2015 23:00:54 +0200

golang-google-appengine (0.0~git20150606-1) unstable; urgency=medium

  * Initial release (Closes: #788655)

 -- Michael Stapelberg <stapelberg@debian.org>  Sat, 13 Jun 2015 16:00:39 +0200
